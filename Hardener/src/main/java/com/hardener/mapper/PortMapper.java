package com.hardener.mapper;

import com.hardener.model.Port;
import com.hardener.model.PortDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PortMapper {

    public List<PortDTO> map(Iterable<Port> rawData) {
        List<PortDTO> output = new ArrayList<>();
        for (Port dbPort : rawData) {
            // TODO Could use builder here
            PortDTO portDTO = new PortDTO();
            portDTO.setPortNumber(dbPort.getPortNumber());
            portDTO.setStatus(dbPort.getStatus());
            portDTO.setNote(dbPort.getNote());
            output.add(portDTO);
        }
        return output;
    }
}
