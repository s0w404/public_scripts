package com.hardener.repository;

import com.hardener.mapper.PortMapper;
import com.hardener.model.Port;
import com.hardener.model.PortDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository("portRepository")
public class PortReporisotyImpl implements PortRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PortMapper portMapper;

    public PortReporisotyImpl() {
    }



    @Override
    public List<Port> findByPortNumber(Integer portNumber) {
        return null;
    }

    @Override
    public List<PortDTO> findAll() {
        Query query = entityManager.createNamedQuery("getAllPortStatus");
        List<PortDTO> portDtos = portMapper.map(query.getResultList());
        return portDtos;
    }

    @Transactional
    @Override
    public void save(Port port) {
        entityManager.persist(port);
    }
}
