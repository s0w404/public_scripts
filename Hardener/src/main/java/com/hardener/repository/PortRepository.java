package com.hardener.repository;


import com.hardener.model.Port;
import com.hardener.model.PortDTO;

import java.util.List;

public interface PortRepository {
    List<Port> findByPortNumber(Integer portNumber);

    List<PortDTO> findAll();

    void save(Port port);
}
