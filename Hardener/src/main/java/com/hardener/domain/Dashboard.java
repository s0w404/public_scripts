package com.hardener.domain;

public class Dashboard {
    private final String osName;
    private final String osVer;
    private final String userName;

    public Dashboard(String osName, String osVer, String userName) {
        this.osName = osName;
        this.osVer = osVer;
        this.userName = userName;
    }

    public String getOsName() {
        return osName;
    }

    public String getOsVer() {
        return osVer;
    }

    public String getUserName() {
        return userName;
    }
}
