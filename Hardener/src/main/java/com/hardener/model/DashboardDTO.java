package com.hardener.model;

import java.util.List;
import java.util.Map;

public class DashboardDTO {
    private Map<String, String> systemInfo;
    private List<PortDTO> openPorts;

    public DashboardDTO(Map<String, String> systemInfo, List<PortDTO> openPorts) {
        this.systemInfo = systemInfo;
        this.openPorts = openPorts;
    }

    public Map<String, String> getSystemInfo() {
        return systemInfo;
    }

    public void setSystemInfo(Map<String, String> systemInfo) {
        this.systemInfo = systemInfo;
    }

    public List<PortDTO> getOpenPorts() {
        return openPorts;
    }

    public void setOpenPorts(List<PortDTO> openPorts) {
        this.openPorts = openPorts;
    }

    public static class DashboardDTOBuilder {
        private Map<String, String> systemInfo;
        private List<PortDTO> openPorts;

        public DashboardDTOBuilder withSystemInfo(Map<String, String> systemInfo) {
            this.systemInfo = systemInfo;
            return this;
        }

        public DashboardDTOBuilder withOpenPorts(List<PortDTO> openPorts) {
            this.openPorts = openPorts;
            return this;
        }

        public DashboardDTO build() {
            return new DashboardDTO(systemInfo, openPorts);
        }

    }
}


