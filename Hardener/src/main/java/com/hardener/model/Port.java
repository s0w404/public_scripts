package com.hardener.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@NamedQueries(
        @NamedQuery(name = "getAllPortStatus", query = "SELECT p FROM Port AS p")
)

@Entity
@Table(name = "ports")
public class Port implements Serializable {
    private static final long seialVersionUID = 12345;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "portNumber")
    private Integer portNumber;

    @Column(name = "status")
    private String status;

    @Column(name = "note")
    private String note;

    @Column(name = "lastScan")
    private Date lastScan;

    public Port() {
    }

    public Port(Integer portNumber, String status, String note, Date lastScan) {
        this.portNumber = portNumber;
        this.status = status;
        this.note = note;
        this.lastScan = lastScan;
    }

    public Long getId() {
        return id;
    }

    public Integer getPortNumber() {
        return portNumber;
    }

    public String getStatus() {
        return status;
    }

    public String getNote() {
        return note;
    }

    public Date getLastScan() {
        return lastScan;
    }
}
