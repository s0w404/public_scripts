package com.hardener.model;



public class PortDTO {
    private Integer portNumber;
    private String Status;
    private String note;

    public enum PORT_STATUS {
        OPEN, CLOSED, FILTERED
    }

    public Integer getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(Integer portNumber) {
        this.portNumber = portNumber;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
