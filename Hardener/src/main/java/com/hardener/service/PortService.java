package com.hardener.service;

import com.hardener.mapper.PortMapper;
import com.hardener.model.Port;
import com.hardener.model.PortDTO;
import com.hardener.repository.PortRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class PortService {

    @Autowired
    PortRepository portRepository;

    @Autowired
    PortMapper portMapper;


    @RequestMapping("/port")
    public String populate() {
        portRepository.save(new Port(80, null, "TEST", null));
        List<PortDTO> all = portRepository.findAll();
        return "Done";
    }

    @RequestMapping("/portfind")
    public ResponseEntity findAllPorts() {
        Iterable<PortDTO> result = portRepository.findAll();
        return ResponseEntity.ok(result);
    }

    @RequestMapping("/getAllPortStatus")
    public ResponseEntity getAllPortStatus() {
        List<PortDTO> allEntities = portRepository.findAll();
        return ResponseEntity.ok(allEntities);
    }
}
