package com.hardener.service;

import com.hardener.model.PortDTO;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ScanServiceImpl implements ScanService {
    private static final String PATH_TO_SCTIPT = "src/main/resources/scripts/shell/";

    private static final String SCRIPTNAME_SCAN_TOP_100 = "portscan_top100.sh";

    public ScanServiceImpl() {
    }

    @Override
    public List<String> scanPorts() {
        lunchScript(PATH_TO_SCTIPT + SCRIPTNAME_SCAN_TOP_100);
        List<String> openPorts = readOpenedPorts();

        return openPorts;
    }

    @Override
    public List<String> scanTop100Ports() {
        return null;
    }




    private void lunchScript(String pathToScript) {
        try { Process process = Runtime.getRuntime().exec(pathToScript);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> readOpenedPorts() {
        String fileName = "result.txt";
        List<String> result = new ArrayList<>();

        try(Stream<String> stream = Files.lines(Paths.get(fileName))) {
            result = stream
                    .filter(line -> line.endsWith("OPEN"))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("Exception while reading file!");
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<PortDTO> mapToObjects(List<String> rawString) {
        List<PortDTO> portDTOList = new ArrayList<>();
        for (String line : rawString) {
            PortDTO portDTO = new PortDTO();
            String[] result = line.split("\t");

            portDTO.setPortNumber(Integer.parseInt(result[0]));
            if ("CLOSED".equals(result[1])) {
                portDTO.setStatus(PortDTO.PORT_STATUS.CLOSED.toString());
            } else if ("OPEN".equals(result[1])) {
                portDTO.setStatus(PortDTO.PORT_STATUS.OPEN.toString());
            } else {
                portDTO.setStatus(PortDTO.PORT_STATUS.FILTERED.toString());
            }
            portDTOList.add(portDTO);
        }
        return portDTOList;
    }
}
