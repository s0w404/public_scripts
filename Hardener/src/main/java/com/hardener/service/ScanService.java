package com.hardener.service;

import com.hardener.model.PortDTO;

import java.util.List;

public interface ScanService {
    List<String> scanPorts();
    List<String> scanTop100Ports();
    List<PortDTO> mapToObjects(List<String> rawInput);

}
