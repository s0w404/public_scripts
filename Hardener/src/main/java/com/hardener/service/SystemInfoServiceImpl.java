package com.hardener.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class SystemInfoServiceImpl implements SystemInfoService {

    @Override
    public Map<String, String> getSystemInfo() {
        Map<String, String> systemInfo = new HashMap<>();
        systemInfo.put("OS architecture", System.getProperty("os.arch"));
        systemInfo.put("OS Name", System.getProperty("os.name"));
        systemInfo.put("OS Version", System.getProperty("os.version"));
        systemInfo.put("User name", System.getProperty("user.name"));

        return systemInfo;
    }
}
