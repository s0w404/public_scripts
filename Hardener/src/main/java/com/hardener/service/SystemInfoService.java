package com.hardener.service;

import java.util.Map;

public interface SystemInfoService {
    Map<String, String> getSystemInfo();
}
