package com.hardener.web;

import com.hardener.mapper.PortMapper;
import com.hardener.model.DashboardDTO;
import com.hardener.model.PortDTO;
import com.hardener.service.PortService;
import com.hardener.service.ScanService;
import com.hardener.service.SystemInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@Controller
public class DashboardController {

    @Autowired
    private SystemInfoService systemInfoService;
    @Autowired
    private ScanService scanService;

    // SystemInfoService:       OK
    // PortScanService:         OK
    // RequiredProgramsService  MISSING
    // SystemHardenerService    MISSING
    // PendingTaskService       MISSING
    //

    @RequestMapping(value = "/")
    public ResponseEntity<DashboardDTO> getDashboard() {
        Map<String, String> systemInfo = systemInfoService.getSystemInfo();
        List<String> openPorts = scanService.scanPorts();
        List<PortDTO> ports = scanService.mapToObjects(openPorts);

        DashboardDTO dashboardDTO = new DashboardDTO.DashboardDTOBuilder()
                .withSystemInfo(systemInfo)
                .withOpenPorts(ports)
                .build();

        return ResponseEntity.ok(dashboardDTO);
    }
}
