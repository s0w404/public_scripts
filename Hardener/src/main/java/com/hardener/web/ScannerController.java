package com.hardener.web;

import com.hardener.service.ScanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class ScannerController {

    @Autowired
    private ScanService scanService;

    @RequestMapping("/scan")
    public String getScan(Model model) {
        List<String> openPorts = scanService.scanPorts();
        model.addAttribute("openPorts", openPorts);

        return "scanResult";
    }
}
