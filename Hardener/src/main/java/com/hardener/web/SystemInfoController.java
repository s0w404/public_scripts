package com.hardener.web;

import com.hardener.service.SystemInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class SystemInfoController {

    @Autowired
    private SystemInfoService systemInfoService;

    @RequestMapping("/info")
    public String getSystemInfo(Model model) {
        Map<String, String> systemInfo = systemInfoService.getSystemInfo();
        model.addAllAttributes(systemInfo);
        return "systemInfo";
    }
}
