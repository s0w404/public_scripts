package com.hardener.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

@Controller
public class GreetingController {

    private static final String TEMPLATE = "Hello, %s";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping(value = "/greeting")
    public String getGreeting(@RequestParam(value = "name", defaultValue = "world") String name, Model model) {

        Properties properties = System.getProperties();
        model.addAttribute("name", name);
        model.addAttribute("properties", properties);
        return "index";
    }

    @GetMapping(value = "/scan2")
    public String getScanPorts(Model model) {
        String[] cmd = new String[]{"/home/hackmehard/Shell_Scripts/Public/Port-hardener/modules/portscan_top100.sh"};
        try {
            Process proc = Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileInputStream inputStream = new FileInputStream("danger.txt");
            String everything = org.apache.commons.io.IOUtils.toString(inputStream);
            model.addAttribute("result", everything);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        return "dashboard";
    }
}
