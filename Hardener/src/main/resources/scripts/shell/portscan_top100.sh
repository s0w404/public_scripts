#!/bin/bash

# Author:   Pawel Maryszczuk
# Email:    w1ld_0w1@protonmail.com
# Date:     10.07.2018
# Usage:    ./portscan_top100.sh
# Description:
##################################################
# Module intended to check top-100 nmap port's.  #
# Can be used as separate script                 #
# ################################################

# CONSTANS_DECLARATION
MODULE_NAME="Port Scam - TOP 100"
readonly PORTS=(7 9 13 21 22 23 25 26 37 53 79 80 81 88 106 110 111 113 119 135 139 143 144 179 199 389 427 443 444 445 465 513 514 515 543 544 548 554 587 631 646 873 990 993 995 1025 1026 1027 1028 1029 1110 1433 1720 1723 1755 1900 2000 2001 2049 2121 2717 3000 3128 3306 3389 3986 4899 5000 5009 5051 5060 5101 5190 5357 5432 5631 5666 5800 5900 6000 6001 6646 7070 8000 8008 8009 8080 8081 8443 8888 9100 9999 10000 32768 49152 49153 49154 49155 49156 49157)
readonly RED='\e[0;31m'
readonly GREEN='\e[0;32m'
readonly NC='\e[0m'
#readonly FORMAT="%-10s %05d %10s\t\t\t %10s"  // For later use, when PRINTF will be used.

E_NOROOT=10
E_FILEERR=11

OPEN_PORTS=()
VULN_PORTS_COUNTER=0


function hello() {
  echo "Module:     $MODULE_NAME"
  echo ""
  echo ""
}

function parseTCP() {
  local return_value="$(echo $1 | awk '{print $4}' | cut -d":" -f2)"
  port=$return_value
}

function parseTCP6() {
  local return_value="$(echo $1 | awk '{print $4}' | rev | cut -d":" -f1 | rev)"
  port=$return_value
}

function getAllOpenPort() {
  IFS=
  RESULTX="$(netstat -plnt | tail -n+3)" # Tailing to delete header of netstat command
  echo $RESULTX > temp_scan.txt
  if [[ $? -ne 0 ]]; then
    echo "Error while creating file! Check write permision"
    exit E_FILEERR
  fi
}

function parseFile() {
  cat temp_scan.txt | while read -r line;
  do
    #port="$(echo $line | awk '{print $4}' | cut -d":" -f2)"
    local protocol="$(echo $line | awk '{print $1}')"
    if [ $protocol = "tcp" ]; then
      parseTCP $line
    elif [ $protocol = "tcp6" ]; then
      parseTCP6 $line
    fi
    OPEN_PORTS+="$port "
    echo "${OPEN_PORTS[*]}" > openports.txt
  done
}

function checkPorts() {
  echo -n "" > danger.txt
  echo -n "" > result.txt # Clear log file
  readarray -d " " OPEN_PORTS < openports.txt
  local sizeOfOpen=${#OPEN_PORTS[@]}

  for i in "${PORTS[@]}"; do
    COUNTER=1
    while [[ $COUNTER -le $sizeOfOpen ]]; do
      if [[ ${OPEN_PORTS[$COUNTER]} -eq $i ]]; then
        echo -e "PORT NUMBER: $i status:         OPEN" >> danger.txt
        echo -e "$i\tOPEN" >> result.txt
        let VULN_PORTS_COUNTER=VULN_PORTS_COUNTER+1
        break
      fi
     let COUNTER=COUNTER+1
    done
    echo -e "$i\tCLOSED" >> result.txt
  done
}

function displayResult() {
  column result.txt
}

function sumarize() {
  echo ""
  echo ""
  echo ""
  echo -e "Scan complete! Vulnerable port's found: ${RED}$VULN_PORTS_COUNTER${NC}"
  echo "You can run 'cat danger.txt to see them!'"
}

## TESTGROUND
hello
getAllOpenPort
parseFile
checkPorts
displayResult
sumarize
