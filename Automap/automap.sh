#!/bin/bash

#TODO
# * Send log to remote server
# * Decision should i keep them locally, or delete right after (We can keep them, we got file handle)
# * Configure some safe FTP where i can store logs

LOG_FILENAME=""

function displayInfo() {
  echo ""
  echo ""
  echo "====================================="
  echo "   S C A N   C O M P L E T E        ="
  echo "====================================="
  echo "Scanned ip:     $SCAN_IP"
  echo "IP in File:     `cat lastip.txt`"

}

function checkIpMasking() {
  echo "Checking if IP adress is diffrent then native one..."
  local my_ip=$(curl ifconfig.co)
  local my_ip_masked=$(proxychains curl ipconfig.co | grep "Your IP" | cut -c 56- | rev | cut -c 8- | rev)
  echo "Recived IP adress: $my_ip and masked is $my_ip_masked"
  if [ $my_ip != $my_ip_masked ]; then
    isMasked=true
  else
    isMasked=false
  fi
}

function panicWhenSameIp() {
  if [ isMasked = false ]; then
    echo "PANIC! GOT SAME IP ADRESS! DAMN MAN, FIX PROXY"
    exit 1
  fi
}

function parseNmapIpAdress() {
  IP_CHANGE_FLAG="0"
  local lastScannedIp=`cat lastip.txt`
  local offet=3 # Range in witch script will scan, e.g. 5 means 127.0.0.1-5
  local lastOctet=`echo $lastScannedIp | cut -d'.' -f4 | cut -d'-' -f2`
  if [ $lastOctet = "" ]; then
    lastOctet =`echo $lastScannedIp | cut -d'.' -f4`
  fi
  if [ $lastOctet -gt `expr 255 - $offet` ]; then
    thirdOcted=`echo $lastScannedIp | cut -d'.' -f3`
    seconOcted=`echo $lastScannedIp | cut -d'.' -f2`
    firstOcted=`echo $lastScannedIp | cut -d'.' -f1`
    echo $firstOcted $seconOcted $thirdOcted
    if [ $thirdOcted -gt 253 ]; then
      thirdOcted="0"
      seconOcted=`expr $seconOcted + 1`
      echo $seconOcted
    fi
    if [ $seconOcted -gt 253 ]; then
      seconOcted="0"
      firstOcted`expr $firstOcted + 1`
    fi
    local ipToExport="$firstOcted.$seconOcted.$thirdOcted.$lastOctet"
    echo $ipToExport > "lastip.txt"
    echo $ipToExport
    echo $lastOctet
    IP_CHANGE_FLAG="1"
  fi
  END_RANGE_IP=`expr $lastOctet + $offet`
  if [ $IP_CHANGE_FLAG = "1" ]; then
    END_RANGE_IP="255"
    local ipToExport="$firstOcted.$seconOcted.$thirdOcted.0"
    echo $ipToExport > "lastip.txt"
  fi
  lastOctet=`expr $lastOctet + 1`
  if [ `expr $lastOctet - $offet` -gt 255 ]; then
    lastOctet="254"
  fi
  LAST_OCTET="$lastOctet-$END_RANGE_IP"
  SCAN_IP=`echo $lastScannedIp | cut -d'.' -f1-3`
  SCAN_IP="$SCAN_IP.$LAST_OCTET"
}

function updateVariables() {
  LOG_FILENAME="owl1sh_$SCAN_IP.xml"
}

function executeSafeNmap() {
  if [ isMasked ]; then
    # Uncomment for full-port scan (LONG ONE!)
    # poryxchains4 nmap -sT -PN -n -sV -oX $LOG_FILENAME $SCAN_IP
     proxychains nmap -sT -Pn -n -sV -F -oX $LOG_FILENAME $SCAN_IP
  fi
}

function checkIfLogFileExistAndRepeatScanIfNot() {
  if [ ! -f $LOG_FILENAME ]; then
    echo "WARNING! LOG FILE NOT FOUND! REPEATING SCAN..."
    executeSafeNmap
    checkIfLogFileExistAndRepeatScanIfNot
  fi
  echo "File saved propetly..."
}

function updateLastScannedAddress() {
  echo "Updating file"
  `echo $SCAN_IP > lastip.txt`
}

# Main loop, will do it till end of world
while :
do
  checkIpMasking
  panicWhenSameIp
  parseNmapIpAdress
  updateVariables
  executeSafeNmap
  checkIfLogFileExistAndRepeatScanIfNot
  updateLastScannedAddress
  displayInfo
done
