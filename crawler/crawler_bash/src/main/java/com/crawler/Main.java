package com.crawler;


/*
Program execution
    Check does required files present
        Folder with config/ if not create one
        Files with configuration

Classes
    Verifier - verifies does system meet requirment's
    Downloader - Download's project's
    Parser - Parses files and extract info


Test resources should contain some files that will be able to parse and return searched string's
*/

import org.apache.log4j.Logger;
import org.eclipse.jgit.api.Git;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    final static Logger logger = Logger.getLogger(Main.class);

    final static String configFilePath = "CONFIG/config.conf";


    public static void main(String[] args) {

        Verificator verificator = new Verificator();
        List<Path> toParseJava = null;
        List<String> foundPasswords = new ArrayList<>();

        logger.info("Verification of project structure in progress...");
        verificator.checkConfigurationSkeleton();
        logger.info("Verification of project structure finished.");
        List<String> targets = verificator.getTargetsFromConfigFile();

        Downloader downloader = new Downloader();

        for (String target : targets) {
            Git git = downloader.downloadRepository(target);
            try (Stream<Path> pathStream = Files.walk(Paths.get(git.getRepository().getDirectory().getParent()))) {
                logger.info("Collecting .java files to scan...");
                toParseJava = pathStream
                        .filter(s -> s.toString().endsWith(".java"))
                        .collect(Collectors.toList());
                logger.info("Found " + toParseJava.size() + " files to be parsed");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (Path path : toParseJava) {
            try ( Stream<String> stream = Files.lines(path)) {

                logger.info("Parsing file " + path);
                foundPasswords.addAll(stream.filter(s -> s.contains("password"))
                        .collect(Collectors.toList()));
                logger.info("Found " + foundPasswords.size() + " possible passwords");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Found X Potential password's:");
        for (String lane : foundPasswords) {
            System.out.println(lane);
        }
    }

}
