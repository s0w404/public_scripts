package com.crawler;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Verificator {
    final static Logger logger = Logger.getLogger(Verificator.class);

    final String CONFIG_FILE_PATH = "CONFIG/config.conf";



    public Boolean doesFileExist(String path) {
        File file = new File(path);
        if (file.exists() && !file.isDirectory()) {
            return true;
        }
        return false;
    }

    public Boolean doesDirectoryExist(String path) {
        File file = new File(path);
        if (file.exists() && file.isDirectory()) {
            return true;
        }
        return false;
    }

    public void checkConfigurationSkeleton() {
        if (!doesDirectoryExist("CONFIG")) {
            logger.warn("CONFIG directory not found! Creating skeleton for CONFIG directory");
            Path configPath = Paths.get("CONFIG/config.conf");
            try {
                Files.createDirectories(configPath.getParent());
                Files.write(configPath, "PLEASE PROVIDE HERE CONFIGURATION!".getBytes());
            } catch (IOException e) {
                logger.fatal("Error creating CONFIG directory!");
                logger.fatal(e.getMessage());
            }
        }

        if (!doesDirectoryExist("CONTAINER")) {
            logger.warn("CONTAINER directory not found! Creating skeleton for CONTAINER directory");
            Path configPath = Paths.get("CONTAINER");
            try {
                Files.createDirectories(configPath);
            } catch (IOException e) {
                logger.fatal("Error creating CONTAINER directory!");
                logger.fatal(e.getMessage());
            }
        }

        if (!doesDirectoryExist("OUTPUT")) {
            logger.warn("OUTPUT directory not found! Creating skeleton for OUTPUT directory");
            Path configPath = Paths.get("OUTPUT/output.txt");
            try {
                Files.createDirectories(configPath.getParent());
                Files.write(configPath, "HERE YOU WILL SEE EXTRACTED PASSWORD's".getBytes());
            } catch (IOException e) {
                logger.fatal("Error creating OUTPUT directory!");
                logger.fatal(e.getMessage());
            }
        }
    }

    public List<String> getTargetsFromConfigFile() {
        List<String> output = new ArrayList<>();
        File file = new File(CONFIG_FILE_PATH);

        try (Stream<String> stream = Files.lines(Paths.get(CONFIG_FILE_PATH))) {
            output = stream
                    .filter(line -> line.startsWith("https://"))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            logger.fatal("Error while parsing config file!");
            logger.fatal(e.getMessage());
        }

        if (output.isEmpty()) {
            logger.info("Scanning config file gave no result's. Does it contain target's?");
        }

        logger.info("Loaded " + output.size() + " target's");
        return output;
    }
}
