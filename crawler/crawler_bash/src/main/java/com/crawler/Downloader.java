package com.crawler;

import org.apache.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Downloader {
    final static Logger logger = Logger.getLogger(Downloader.class);


    public Git downloadRepository(String target) {
        String projectName = target.substring(target.lastIndexOf("/") +1 );
        File destinationFolder = new File("TARGET/" + projectName);
        Git git = null;

        logger.info("Attempting to download repository " + target);
        try {
            git = Git.cloneRepository()
                    .setURI(target)
                    .setDirectory(destinationFolder)
                    .call();
        } catch (GitAPIException e) {
            logger.fatal("Exception while downloading repository: " +target);
            logger.fatal(e.getMessage());
        }
        logger.info("Succefully downloaded repository! Returning handler... " + git.toString());
        return git;
    }

    private List<Path> getFilesToParse(String extension, Git repository) {
        Path parentDirectory = Paths.get(repository.getRepository().getDirectory().getParent());

        try (Stream<Path> pathStream = Files.walk(parentDirectory)) {
            logger.info("Extracting files with extension " + extension + " from " + repository.toString());
            return pathStream
                    .filter(s -> s.toString().endsWith(extension))
                    .collect(Collectors.toList());

        } catch (IOException e) {
            logger.fatal("FATAL ERROR while extracting " + extension);
            logger.fatal(e.getMessage());
        }
        return Collections.emptyList();
    }
}
