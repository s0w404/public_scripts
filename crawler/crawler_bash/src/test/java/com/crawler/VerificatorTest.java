package com.crawler;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

class VerificatorTest {
    private final static String PATH = "Test/config.conf";

    @BeforeAll
    public static void setUp() {
        Path path = Paths.get("Test/config.conf");
        try {
            Files.createDirectories(path.getParent());
            Files.write(path, "TESTING CAN BE FUN".getBytes());
            Files.write(path, "http://testrepository.git".getBytes());
        } catch (IOException e) {
            System.out.println("Failded to create directory on setup!");
        }

    }

    @org.junit.jupiter.api.Test
    void doesFileExist() {
        // Given
        Verificator verificator = new Verificator();

        // When

        String pathToFile = "Test/config.conf";

        // Then
        assertTrue(verificator.doesFileExist(pathToFile));
    }

    @org.junit.jupiter.api.Test
    void doesDirectoryExist() {
        // Given
        Verificator verificator = new Verificator();

        // When
        String pathToDirectory = "Test";

        // Then
        assertTrue(verificator.doesDirectoryExist(pathToDirectory));
    }

    @AfterAll
    public static void cleanUp() {
        File file = new File(PATH);

        if (file.delete()) {
            System.out.println("Deleted file properly!");
        } else {
            System.out.println("Failed to delete directory...");
        }
    }
}