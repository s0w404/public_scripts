package com.hackmehard.crawler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeedStarterService {

    @Autowired
    private SeedStarterRepository seedStarterRepository;

    public List<SeedStarter> findAll() {
        return this.seedStarterRepository.findALl();
    }

    public void add(final SeedStarter seedStarter) {
        this.seedStarterRepository.add(seedStarter);
    }
}
