# Wild Owl's Library

Public script's i wrote, mostly for learning purpose

## Current scripts

* automap - scan network and stay anonymous. Wrapper to check before scan, are you behind TOR.
* Port-hardener - Check your system are you resistant to Port-scanning! Check's mostly scanned port's by Nmap

## Authors

* **Pawel Maryszczuk** - W1ld_0w1@protonmail.com

## License

This project is licensed under the **Pirate-Style License.** Just rob it and do whatever you want with it. ARRR

## Acknowledgments

Writing all those little buddys gave me a lot of fun, but i cannot assure you they will work on Production environment. Actually, i STRONGLY don't recommend to lunch them anywhere else, then on private computers.

